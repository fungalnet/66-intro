# 66-intro

Introduction and quick reference guide to s6/66 system management.
What everyone must know after installing 66, how to boot and configure a system.